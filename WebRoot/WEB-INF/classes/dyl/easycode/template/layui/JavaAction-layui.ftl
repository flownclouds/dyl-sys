<#macro selectolums><#list table.columns as c><#if c_has_next>${c.columnName},<#else>${c.columnName}</#if></#list></#macro>
<#macro insertFlag><#list table.baseColumns as c><#if c_has_next>?,<#else>?</#if></#list></#macro>
<#macro updateColums><#list table.baseColumns as c><#if c_has_next>${c.columnName}=?,<#else>${c.columnName}=?</#if></#list></#macro>
<#macro updateVals><#list table.baseColumns as c><#if c_has_next>${table.javaProperty}.get${c.javaPropertyForGetSet}(),<#else>${table.javaProperty}.get${c.javaPropertyForGetSet}()</#if></#list></#macro>

<#list table.importList as i>
import ${i};
</#list>
import javax.annotation.Resource;

import dyl.common.util.JdbcTemplateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import dyl.sys.action.BaseAction;
import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.Page;
import javax.servlet.http.HttpServletRequest;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * ${sysTime}
 */
@Controller
public class ${table.className}Action extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private ${table.className}ServiceImpl ${table.javaProperty}ServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@RequestMapping(value = "/${table.javaProperty}!main.do")
	public String main(Page page,${table.className} ${table.javaProperty},HttpServletRequest request){
		try{
			request.setAttribute("${table.javaProperty}List",${table.javaProperty}ServiceImpl.find${table.className}List(page, ${table.javaProperty}));
			request.setAttribute("${table.javaProperty}", ${table.javaProperty});
			<#list table.columns as c>
			<#if c.selectSql??>
			request.setAttribute("${c.javaProperty}List",jdbcTemplate.queryForList("${c.selectSql}"));	
			</#if>
			</#list>
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/{todo}/${table.javaProperty}/${table.javaProperty}Main";
	}
	<#if hasEdit?? && hasEdit == 'true'>
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	@RequestMapping(value = "/${table.javaProperty}!${table.javaProperty}Form.do")
	public String  ${table.javaProperty}Form(${table.primaryKey.javaType} ${table.primaryKey.javaProperty},HttpServletRequest request){
		try {
			if(${table.primaryKey.javaProperty}!=null)request.setAttribute("${table.javaProperty}",${table.javaProperty}ServiceImpl.get${table.className}(${table.primaryKey.javaProperty}));
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/{todo}/${table.javaProperty}/${table.javaProperty}Form";
	}
	/**
	 * 说明：插入${table.tableName}
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!add.do")
	public R add(${table.className} ${table.javaProperty},HttpServletRequest request){
		try {
			//${table.javaProperty}.setCreator(getSysUser(request).getId());
			int ret = ${table.javaProperty}ServiceImpl.insert${table.className}(${table.javaProperty});
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新${table.tableName}
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!update.do")
	public R update(${table.className} ${table.javaProperty},HttpServletRequest request){
		try {
			int ret = ${table.javaProperty}ServiceImpl.update${table.className}(${table.javaProperty});
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	</#if>
	<#if hasDelete?? && hasDelete == 'true'>
	/**
	 * 说明：根据主键删除表${table.tableName}中的记录
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!delete.do")
	public R delete${table.className}(String dataIds,HttpServletRequest request){
		try {
			int[] ret = ${table.javaProperty}ServiceImpl.delete${table.className}(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	</#if>
}
