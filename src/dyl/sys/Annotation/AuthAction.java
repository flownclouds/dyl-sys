package dyl.sys.Annotation;

public enum AuthAction {
	VIEW("查看", 1),
	ADD("添加", 2),
	UPDATE("修改", 3), 
	DELETE("删除", 4);
	private String name ;
    private int index ;
    private AuthAction( String name,int index){
        this.name = name ;
        this.index = index ;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
}
